# Slack contacts to google group import

This is a script running on serverless platform - [Syncano](https://syncano.io/#/) to import contacts from slack to google directory group.
To get more information on Syncano go to docummentation page [here](https://docs.syncano.io/#/).

## Setup

There are couple of enviroment variables to set up which are described in `./syncano/explorer-contacts/socket.yml` file.

# Structure

The app is using one scoket - `explorer-contacts` which has multiple endpoints described in `./syncano/explorer-contacts/socket.yml` file.

# Running an export

The application can be runned by sending `GET` request on `/export` endpoint or by using a slash command in slack `/exportcontacts`.