import Syncano from '@syncano/core';
import * as request from 'request';

const clientId = CONFIG.SLACK_CLIENT_ID;
const clientSecret = CONFIG.SLACK_CLIENT_SECRET;

export default (ctx) => {
  const { response, logger } = new Syncano(ctx);
  const log = logger('endpoints.slack-oauth');
  const { code } = ctx.args;

  if (!code) {
    response.status(500);
    response.send({ Error: "Looks like we're not getting code." });
    log.error("Looks like we're not getting code.");
  } else {
    // If it's there...

    /*
      We'll do a GET call to Slack's `oauth.access` endpoint, passing our app's client ID,
      client secret, and the code we just got as query parameters.
    */
    request({
      url: 'https://slack.com/api/oauth.access', // URL to hit
      qs: { code, client_id: clientId, client_secret: clientSecret }, // Query string data
      method: 'GET', // Specify the method
    }, (error, res, body) => {
      if (error) {
        log.error(error);
      } else {
        res.json(body);
      }
    });
  }
};
