import * as request from 'request-promise';

export default class SlackService {
  constructor({ SLACK_TOKEN, SLACK_CHANNEL_ID }) {
    this.apiKey = SLACK_TOKEN;
    this.channelId = SLACK_CHANNEL_ID;
    this.slackApiUrl = 'https://slack.com/api';
  }

  async getChannelMembers(channelId) {
    const slackResponse = await request.get(`${this.slackApiUrl}/channels.info?token=${this.apiKey}&channel=${channelId}`);
    return JSON.parse(slackResponse).channel.members;
  }

  async getUsers() {
    const slackResponse = await request.get(`${this.slackApiUrl}/users.list?token=${this.apiKey}`);
    return JSON.parse(slackResponse).members;
  }

  async getActiveUsers() {
    const users = await this.getUsers();
    const channelMembers = await this.getChannelMembers(this.channelId);

    return users.filter((member) => {
      const isChannelMember = channelMembers.includes(member.id);
      return !member.is_bot && !member.deleted && member.profile.email && isChannelMember;
    })
      .map((member) => ({
        email: member.profile.email,
        role: 'MEMBER',
      }));
  }
}
