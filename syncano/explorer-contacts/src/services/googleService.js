import { google } from 'googleapis';

export default class GoogleService {
  constructor(config) {
    this.serviceAccountEmail = config.SERVICE_ACCOUNT_EMAIL;
    this.serviceAccountKey = config.SERVICE_ACCOUNT_KEY.replace(/\\n/g, '\n');
    this.userEmail = config.USER_EMAIL;
    this.groupKey = config.GROUP_KEY;
    this.SCOPES = [
      'https://www.googleapis.com/auth/admin.directory.group',
    ];
  }

  async initialize() {
    this.auth = await this.authenticate();
    this.directoryService = google.admin({ version: 'directory_v1', auth: this.auth });
  }

  randomWait(time) {
    return new Promise((resolve) => {
      const random = Math.floor(Math.random() * time);
      setTimeout(() => resolve(), random);
    });
  }

  async getMembers(pageToken, previousMembers = []) {
    const response = await this.directoryService.members.list({
      groupKey: this.groupKey,
      pageToken,
    });
    const { data } = response;
    if (data.nextPageToken) {
      return this.getMembers(data.nextPageToken, [...data.members, ...previousMembers]);
    }

    return [...response.data.members, ...previousMembers];
  }

  async insertMember(member) {
    try {
      await this.directoryService.members.insert({
        groupKey: this.groupKey,
        requestBody: member,
      });
    } catch (err) {
      if (err.message === 'Member already exists.') {
        return;
      }

      if (err.message === 'Request rate higher than configured.') {
        await this.randomWait(2 * 1000);
        return this.insertMember(member, this.auth);
      }

      throw err;
    }
  }

  async deleteMember(member) {
    try {
      await this.directoryService.members.delete({
        groupKey: this.groupKey,
        memberKey: member.id,
      });
    } catch (err) {
      if (err.message === 'Resource Not Found: memberKey') {
        return;
      }

      if (err.message === 'Request rate higher than configured.') {
        await this.randomWait(2 * 1000);
        return this.deleteMember(member);
      }


      throw err;
    }
  }

  async authenticate() {
    return new Promise((resolve, reject) => {
      const auth = new google.auth.JWT(
        this.serviceAccountEmail,
        null,
        this.serviceAccountKey,
        this.SCOPES,
        this.userEmail,
      );

      auth.authorize((err) => {
        if (err) reject(err);
        resolve(auth);
      });
    });
  }
}
