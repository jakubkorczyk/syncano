import Syncano from '@syncano/core';

export default async (ctx) => {
  const { response, data } = new Syncano(ctx);
  const { jobName } = ctx.args;

  try {
    const job = await data.job.where('jobName', jobName).first();
    response.json({
      job,
    }, 200);
  } catch (err) {
    response.json({
      message: err.message,
    }, 500);
  }
};
