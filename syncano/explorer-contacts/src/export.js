import Syncano from '@syncano/core';

import SlackService from './services/slackService';

export default async (ctx) => {
  const {
    response, event, data, logger,
  } = new Syncano(ctx);
  const log = logger('endpoints.export');

  try {
    const slackService = new SlackService(CONFIG);
    const users = await slackService.getActiveUsers();
    const jobName = (+new Date()).toString(36);

    await data.job.updateOrCreate({
      jobName,
    }, {
      status: 'PROCESSING',
      jobName,
    });
    log.info(`Got ${users.length} slack members, emmiting 'members' event.`);
    event.emit('members', { members: users, jobName });

    response.json({
      data: {
        count: users.length,
        message: 'Request has been accepted, result can be found under the given url.',
        url: `https://api.syncano.io/v2/instances/snowy-mountain-1222/endpoints/sockets/explorer-contacts/job-result/?jobName=${jobName}`,
        jobName,
      },
    }, 202);
  } catch (err) {
    log.error(`Error while processing 'export' request: ${err}`);
    response.json({
      message: err.message,
    }, 500);
  }
};
