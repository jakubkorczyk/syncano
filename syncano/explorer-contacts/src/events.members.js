import Syncano from '@syncano/core';
import GoogleService from './services/googleService';

export default async (ctx) => {
  const { data, logger } = new Syncano(ctx);
  const log = logger('events.members');
  const { members, jobName } = ARGS;
  const googleService = new GoogleService(CONFIG);

  try {
    await googleService.initialize();
    const googleGroupMembers = await googleService.getMembers();
    const toDelete = googleGroupMembers.filter((gMember) => !members.some((member) => member.email === gMember.email));

    const insertPromises = members.map((member) => googleService.insertMember(member));
    const deletePromises = toDelete.map((member) => googleService.deleteMember(member));

    await Promise.all(insertPromises);
    await Promise.all(deletePromises);
    log.info(`${members.length} members successfully saved to google groups, ${toDelete.length} deleted.`);

    await data.job.updateOrCreate({
      jobName,
    }, {
      status: 'SUCCESS',
    });
  } catch (err) {
    log.error(`Error while processing export ${jobName}, \nError: ${err}`);
    await data.job.updateOrCreate({
      jobName,
    }, {
      status: 'ERROR',
      error: err.message,
    });
  }
};
